export class Injury {
    dateOfInjury : any;
    injuryType: string;

    constructor(dateOfInjury : any, injuryType : string){
        this.dateOfInjury = dateOfInjury;
        this.injuryType = injuryType;
    }

}
