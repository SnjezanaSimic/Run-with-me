package com.app.rwm.dto;

public class CooperDTO {

	public int distance;

	public CooperDTO(int distance) {
		super();
		this.distance = distance;
	}

	public CooperDTO() {
		super();
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	
}
